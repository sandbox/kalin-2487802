<?php

/**
 * @file
 * Contains \Drupal\entity_hierarchy\Controller\EntityManagerController.
 */

namespace Drupal\entity_hierarchy\Controller;

/**
 * Controller routines for entity_hierarchy routes.
 */
class EntityManagerController {
  
  public function getList() {

    $entity_list = array(
      'article' => array(
        '#type' => 'markup',
        '#markup' => "<a href='/admin/structure/entity_hierarchy/article'>Article</a>"
      ),
      'tags' => array(
        '#type' => 'markup',
        '#markup' => "<a href='/admin/structure/entity_hierarchy/tags'>Tags</a>"
      ),
    );

    $build['entity_list'] = array(
      '#theme' => 'item_list',
      '#items' => $entity_list,
      '#title' => t('Entities'),
    );

    return $build;
  }

}

