<?php

/**
 * @file
 * Contains \Drupal\entity_hierarchy\Form\EntityManagerSettingsForm.
 */

namespace Drupal\entity_hierarchy\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\True;

/**
 * Selecting which content of which entity type will be managed in herarchy.
 */
class EntityManagerSettingsForm extends FormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a EntityManagerSettingsForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_types = $this->entityManager->getDefinitions();
    $bundles = $this->entityManager->getAllBundleInfo();
    $labels = array();

    foreach ($entity_types as $entity_type_id => $entity_type) {
      if ($entity_type instanceof ContentEntityTypeInterface) {
        $labels[$entity_type_id] = $entity_type->getLabel() ?: $entity_type_id;
      }
    }

    asort($labels);

    $form = array('#labels' => $labels);

    $form['entity_types'] = array(
      '#title' => $this->t('Content entities that could be managed in hierarchy.'),
      '#type' => 'fieldset',
    );

    foreach ($labels as $entity_type_id => $entity_type_label) {
      $form['entity_types'][$entity_type_id] = array(
        '#title' => $entity_type_label,
        '#type' => 'details',
        '#open' => TRUE,
      );
      $bundle_types = $bundles[$entity_type_id];
      foreach ($bundle_types as $type) {
        $form['entity_types'][$entity_type_id][] = array(
          '#title' => $type['label'],
          '#type' => 'checkbox',
        );
      }
    }

    $form['settings'] = array('#tree' => TRUE);

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {}

  public function submitForm(array &$form, FormStateInterface $form_state) {}

  public function getFormId() {
    return 'entity_hierarchy.settings_form';
  }
}
