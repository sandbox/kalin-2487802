<?php

/**
 * @file
 * Contains \Drupal\entity_hierarchy\Form\EntityManagerForm.
 */

namespace Drupal\entity_hierarchy\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class EntityManagerForm extends FormBase {
  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\EntityInterface $entity_name
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $entity_name = NULL) {
    $form['wrapper'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Hierarchy manager'),
    );

    return $form;

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  public function getTitle(EntityInterface $entity_name = NULL) {
    return $this->t("Managing hierarchy in $entity_name");
  }


  public function getFormId() {
    return 'entity_hierarchy.entity_management_form';
  }

}
